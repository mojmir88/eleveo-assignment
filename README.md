# Eleveo assignment



## Description

This repository is comprised of four parts.

1. Run docker web application written in python, that displays web page visit counter
   - This simple application is using python Flask framewok and basic count to display how many times was the web app visited.
     Details can be found in `docker_web_app` directory
   - To run the application in docker container use `docker run -dp 80:80 mojmir88/eleveo-assignment` command. The container should run
     on all network interfaces. Open a web browser and enter for example `http://localhost:80` or `http://<publicIP>:80`
    
2. Deploy the web application in Kubernetes
   - To deploy the application in your kubernetes cluster run `kubectl apply -f kubernetes/pod.yaml` command.
   - Use the Node IP of "web-app pod" with port 30080 in your web browser.
     `http://<NodeIP>:30080`

3. Use ansible playbook to download and install k3s and run the application in it
   - Official GitHub repository https://github.com/k3s-io/k3s-ansible used to download and install k3s
   - The k3s-ansible repository has been modified with new "app" role in site.yml playbook that deploys the web app in k3s
   - Use `ansible-playbook k3s-ansible/site.yml -i k3s-ansible/inventory/my-cluster/hosts.ini` command to install k3s and deploy the web
     app. By default the hosts.ini is configured with **localhost** only and group_vars with **ubuntu** ansible user. Modify these files
     if you want to run the playbook on remote master and other nodes and with different user. See also README in k3s-ansible.
   - Use the same technique as in previous step to verify the web app in web browser.
   
4. Use Terraform to create a VM in cloud and run the ansible playbook during the creation process
   - This terraform module uses AWS Cloud provider to create new VM with minimum resources.
   - In order to use terraform configuration file, you need to provide access and secrete key either as environment variables or using
     AWS-CLI and provide your **keyname** in the main.tf, as well as your private key in the main.tf file and in terraform/ansible/.ssh 
     directory.
   - In terraform directory run `terraform plan --out tfplan` and then `terraform apply "tfplan"`
   - Check that the web app is running in your browser as in previous steps.
