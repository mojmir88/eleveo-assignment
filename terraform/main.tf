variable "awsprops" {
    type = map(string)
    default = {
    region = "eu-north-1"
    vpc = "vpc-05870672e24277113"
    ami = "ami-09e1162c87f73958b"
    itype = "t3.micro"
    subnet = "subnet-0c2b17f0a6d11d355"
    publicip = true
    keyname = "mojmir-openssh"
    secgroupname = "IAC-Sec-Group"
  }
}

provider "aws" {
  region = lookup(var.awsprops, "region")
}

resource "aws_security_group" "project-iac-sg" {
  name = lookup(var.awsprops, "secgroupname")
  description = lookup(var.awsprops, "secgroupname")
  vpc_id = lookup(var.awsprops, "vpc")

  // To Allow SSH Transport
  ingress {
    from_port = 22
    protocol = "tcp"
    to_port = 22
    cidr_blocks = ["0.0.0.0/0"]
  }

  // To Allow Port 80 Transport
  ingress {
    from_port = 80
    protocol = "tcp"
    to_port = 80
    cidr_blocks = ["0.0.0.0/0"]
  }
 
  // To Allow Web-App connection
  ingress {
    from_port = 0
    protocol = "tcp"
    to_port = 30080
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]
  }

  lifecycle {
    create_before_destroy = true
  }
}


resource "aws_instance" "project-iac" {
  ami = lookup(var.awsprops, "ami")
  instance_type = lookup(var.awsprops, "itype")
  subnet_id = lookup(var.awsprops, "subnet") #FFXsubnet2
  associate_public_ip_address = lookup(var.awsprops, "publicip")
  key_name = lookup(var.awsprops, "keyname")


  vpc_security_group_ids = [
    aws_security_group.project-iac-sg.id
  ]
  root_block_device {
    delete_on_termination = true
    volume_size = 20
    volume_type = "gp2"
  }
  tags = {
    Name ="SERVER01"
    Environment = "DEV"
    OS = "UBUNTU"
    Managed = "IAC"
  }

  depends_on = [ aws_security_group.project-iac-sg ]

  connection {
    type = "ssh"
    user = "ubuntu"
    private_key = file("./ansible/.ssh/mojmir-openssh.pem")
    host = "${self.public_ip}"
  }

  provisioner "file" {
    source = "~/eleveo-assignment/k3s-ansible/"
    destination = "/home/ubuntu/k3s-ansible/"
  }

  provisioner "file" {
    source = "./ansible/install.sh"
    destination = "/home/ubuntu/install.sh"
  }

  provisioner "remote-exec" {
    inline = [
      "chmod +x /home/ubuntu/install.sh",
      "/home/ubuntu/install.sh",
    ]
  }
}

output "ec2instance" {
  value = aws_instance.project-iac.public_ip
}
