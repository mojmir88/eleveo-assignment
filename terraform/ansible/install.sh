#!/bin/bash

sudo add-apt-repository --yes --update ppa:ansible/ansible && sleep 78

sudo apt update && sleep 5;

sudo apt install -y ansible;
sleep 10;
echo "ansible installed"

echo "running playbook"
ansible-playbook ~/k3s-ansible/site.yml -i ~/k3s-ansible/inventory/my-cluster/hosts.ini
